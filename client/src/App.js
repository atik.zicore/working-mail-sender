import Home from "./Components/Home";

function App() {
  return (
    <div className="max-w-[700px] mx-auto bg-white">
  <Home/>
    </div>
  );
}

export default App;
