import React, { useState } from "react";
import axios from "axios";

const Home = () => {
    const [email, setEmail] = useState();
    const [name, setName] = useState();
    const [subject, setSubject] = useState();
    const [message, setMessage] = useState();

    const sendMail = (e) => {
            e.preventDefault();
            console.log("Name:", name);
            console.log("Email:", email);
            console.log("Subject:", subject);
            console.log("Message:", message);

            
        axios
        .get("http://localhost:5000/", {
            params: {
                name,
                email,
                subject,
                message
            },
        })
        .then(() => {
            alert("Message Sent");
        })
        .catch(() => {
            alert("Message sending Fail");
        })
    };


  return (
    <section className="w-full h-full">
      <h1 className="bg-gray-600/40 text-center">Recipient Information</h1>

     
       <form onSubmit={sendMail}>
       <div className="flex basis-full flex-1">
          <label htmlFor="recipient_name" className="basis-[30%]">
            Ricipient Name
          </label>
          <input
            type="text"
            placeholder="Enter Name"
            id="recipient_name"
            className="basis-[65%] border border-gray-500/30 p-2 "
            onChange={(e) => setName(e.target.value)}
          />
        </div>

        <div className="flex basis-full flex-1">
          <label htmlFor="recipient_email" className="basis-[30%]">
            Ricipient Email
          </label>
          <input
            type="text"
            placeholder="Enter Email"
            id="recipient_email"
            className="basis-[65%] border border-gray-500/30 p-2 "
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>

        <div className="flex basis-full flex-1">
          <label htmlFor="recipient_subject" className="basis-[30%]">
            Ricipient Subject
          </label>
          <input
            type="text"
            placeholder="Enter Name"
            id="recipient_subject"
            className="basis-[65%] border border-gray-500/30 p-2 "
            onChange={(e) => setSubject(e.target.value)}
          />
        </div>
        <div className="flex basis-full flex-1">
          <label htmlFor="recipient_message" className="basis-[30%]">
            Message
          </label>
          <input
            type="text"
            placeholder="Enter Name"
            id="recipient_message"
            className="basis-[65%] border border-gray-500/30 p-2 "
            onChange={(e) => setMessage(e.target.value)}
          />
        </div>
        <div className="flex justify-center">
          <button type="submit" className="bg-green-500 px-6 py-2 " >
            Send Mail
          </button>
        </div>
       </form>
        {/* <table>
                <tr>
                    <th >Ricipient Name</th>
                    <td>:</td>
                    <td><input type='text' placeholder='Enter Name'/></td>
                </tr>

                <tr>
                    <th >Ricipient Email</th>
                    <td>:</td>
                    <td><input type='email' placeholder='Enter Email'/></td>
                </tr>

                <tr>
                    <th >Mail Subject</th>
                    <td>:</td>
                    <td><input type='text' placeholder='Enter Subject'/></td>
                </tr>

                <tr>
                    <th >Mail Message</th>
                    <td>:</td>
                    <td><input type='text' placeholder='Enter Message'/></td>
                </tr>

                <tr>
                    <th ></th>
                    <td><button>Send Mail</button></td>
                    <td></td>
                </tr>

            </table> */}
     
    </section>
  );
};

export default Home;
